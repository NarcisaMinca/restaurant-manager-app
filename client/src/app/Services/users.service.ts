import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, ResponseContentType, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map'

@Injectable()
export class UserService {

    private headers = new Headers({ 'Content-Type': 'application/json' });
    private url = 'https://app-restaurant-narcisaminca.c9users.io';
    constructor(private http: Http
    ) {}
    
    getAll() {
        return this.http.get(this.url + '/users').map((response: Response) => response.json());
    }
    
    getById(_id: string) {
        return this.http.get(this.url + '/users/current').map((response: Response) => response.json());
    }

    create(user: any) {
        return this.http.post(this.url + '/users/register', user);
    }

    delete(id) {
        return new Promise((resolve, reject) => {
            this.http.delete(this.url + '/users/' + id)
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }
    updateUser(id, data) {
        return new Promise((resolve, reject) => {
            this.http.put(this.url + '/users/' + id, data)
                .map(res => res.json())
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

    forgotPassword(email) {
  
        return new Promise((resolve, reject) => {
            this.http.post(this.url + '/forgotPassword', email)
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }
    resetPassword(data) {

        return new Promise((resolve, reject) => {
            this.http.post(this.url + '/resetPassword', data)
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

}